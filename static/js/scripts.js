// Made a clone of the DOM elements already in the HTML file
// These will act as the template for later
const containerTemplate = document.getElementById("mainContainer").cloneNode(false)
const restaurantTemplate = document.getElementById("mainRestaurant").cloneNode(true)
const reviewTemplate = document.getElementById("mainReview").cloneNode(true)

// Strip the clones' IDs to avoid conflicts
containerTemplate.removeAttribute("id")
restaurantTemplate.removeAttribute("id")
reviewTemplate.removeAttribute("id")

// Remove the main body, leaving the page blank
document.getElementById("mainBody").innerHTML=""

// Make a fetch request to the '/data' end-point
// On success, create a new set of containers from the templates
// for every restaurant
fetch('/data')
  .then(resp => resp.json())
  .catch(error => console.log(error))
  .then(json => json.restaurants.forEach(restaurant => {
    buildNewContainer(restaurant)
  }))

// This function takes JSON data for a restaurant and inserts
// them into templates
function buildNewContainer(restaurant) {
  // Clone a new container from the template
  let container = containerTemplate.cloneNode(false)

  // Clone a new restaurant element from the template
  let restaurantElement = restaurantTemplate.cloneNode(true)


  // Get all span tags
  // These are placeholders for the information we want to display
  let restaurantSpans = restaurantElement.getElementsByTagName('span')

  // Insert data from the JSON into the appropriate span placeholders
  restaurantSpans[0].innerText = restaurant.name
  restaurantSpans[1].innerText = restaurant.location

  // This converts the raing to Unicode "stars"
  restaurantSpans[2].innerText = '★'.repeat(restaurant.rating) + '☆'.repeat(5 - restaurant.rating)

  restaurantSpans[3].innerText = restaurant.description

  // Change the source url of the image
  restaurantElement.getElementsByTagName('img')[0].src = restaurant.image


  // Append the restaurant element to the container
  container.appendChild(restaurantElement)
  

  // For every review...
  restaurant.reviews.forEach(review => {
    // Clone a new review element form the template
    let reviewElement = reviewTemplate.cloneNode(true)

    // Get all span tags
    // These are placeholders for the information we want to display
    let reviewSpans = reviewElement.getElementsByTagName('span')

    // Insert data from the JSON into the appropriate span placeholders
    reviewSpans[0].innerText = review.text
    reviewSpans[1].innerText = review.name

    // Change the hyperlink reference and text
    reviewElement.getElementsByTagName('a')[0].href = review.url
    reviewElement.getElementsByTagName('a')[0].innerText = review.domain
  
    // Append the review element to the container
    container.appendChild(reviewElement)
  });
  
  // Append the container to the main body
  document.getElementById("mainBody").appendChild(container)
}
