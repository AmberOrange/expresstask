// Require necessary dependencies
const express = require("express")
const path = require("path")
// Require the 'data.json' file, which automatically gets imported into a json object
const restaurantJson = require(path.join(__dirname,"resources","data.json"))
// Start a new express app
const app = express()
// Define the port
const port = process.env.PORT || 8080

// Make express use middleware for static distribution and json
app.use(express.static(path.join(__dirname, 'static')))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// Get map root to index.html
app.get('/', (req, res) => {
  console.log("Serving the index page")
  res.status(200).sendFile(path.join(__dirname,"resources","index.html"))
})

// Get map '/data' to the data.json object
app.get('/data', (req, res) => {
  console.log("Getting a data request!")
  res.status(200).json(restaurantJson)
})

// Get map '/restaurant' to restaurant.html
app.get('/restaurant', (req, res) => {
  console.log("Serving the restaurant page")
  res.status(200).sendFile(path.join(__dirname,"resources","restaurant.html"))
})

// Start listening to the port
app.listen(port, () => {
  console.log(`Is now listening to port ${port}`)
})